package com.red.variablecompilacion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.os.BuildCompat;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView tipo;
    private TextView version;
    private TextView tvcomprobvar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        tipo = findViewById(R.id.tvType);
        tipo.setText( "BuildType: " + BuildConfig.BUILD_TYPE);
        version = findViewById(R.id.tvVersion);
        version.setText("Veriion: " + BuildConfig.VERSION_NAME);


        String comprobacion = "";
        switch (BuildConfig.BUILD_TYPE) {
            case "develop":
                comprobacion = "COMPROBADO_DEVELOP";
                break;
            case "release":
                comprobacion = "COMPROBADO_RELEASE";
                break;
            case "debug":
                comprobacion = "COMPROBADO_DEBUG";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + BuildConfig.BUILD_TYPE);

        }
        tvcomprobvar = findViewById(R.id.tvcomprobvar);
        version.setText(comprobacion);





    }
}